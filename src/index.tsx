import React from "react"
import ReactDOM from "react-dom/client"
import { App } from "./containers/Home/index"
import { Accessibility } from "./components/Menu/Accessibility"
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { Rules } from "./containers/Rules"
import { Quiz } from "./containers/Quiz"
import { NotFound } from "./containers/NotFound"
import { Finish } from "./containers/Finish"
import { GlobalStyle } from "./Styled"

const root = ReactDOM.createRoot(document.getElementById("root"))

root.render(
  <BrowserRouter>
    <GlobalStyle />
    <Accessibility />
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="/rules" element={<Rules />} />
      <Route path="/quiz" element={<Quiz />} />
      <Route path="/finish" element={<Finish />} />
      <Route path="/*" element={<NotFound />} />
    </Routes>
  </BrowserRouter>
)
