import { TextField } from "../../components/TextField"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { useForm, Controller } from "react-hook-form"
import { LoginForm } from "./Type"
import { Image, Portal } from "./Styled"
import { Button } from "../../components/Button"
import { useNavigate } from "react-router-dom"
import axios from "axios"

export const Login = () => {
  const navigate = useNavigate()

  const schema = yup.object({
    mail: yup.string().email().required(),
    password: yup.string().required(),
  })

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginForm>({
    resolver: yupResolver(schema),
    defaultValues: {
      mail: "",
      password: "",
    },
  })

  const onSubmit = async (data: LoginForm) => {
    const { mail, password } = data
    await axios
      .post("http://localhost:6001/users/login", {
        mail: mail,
        password: password,
      })
      .then(function (response) {
        if (response.data.status === 200) {
          navigate(`/rules`)
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  }

  return (
    <>
      <Portal>Portail</Portal>
      <Image src="/assets/ecv_digital_logo.png" alt="ecv digital logo" />
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name="mail"
          control={control}
          render={({ field }) => (
            <TextField
              label="Email"
              value={field.value}
              onChange={field.onChange}
              errorMessage={errors.mail?.message}
            />
          )}
        />
        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField
              label="Mot de passe"
              value={field.value}
              onChange={field.onChange}
              type="password"
              errorMessage={errors.password?.message}
            />
          )}
        />
        <Button value={"connexion"} />
      </form>
    </>
  )
}
