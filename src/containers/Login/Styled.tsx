import styled from "styled-components"

export const Image = styled.img`
  width: 300px;
  object-fit: "cover";
  margin-bottom: 24px;
`

export const Portal = styled.h3`
  font-size: 20px;
  margin-bottom: 8px;
`
