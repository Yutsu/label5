import { Radio } from "../../../components/Radio"
import { RadioGroup } from "../../../components/RadioGroup"
import { ContainerQuestionQuiz } from "./Styled"

export const renderRadio = (value: string, onChange: any) => {
  let radioResult = []
  radioResult.push(<p>Pas du tout d'accord</p>)
  for (let i = 0; i <= 5; i++) {
    radioResult.push(
      <RadioGroup value={value} onChange={onChange} key={i}>
        <Radio value={`${i}`}>{i}</Radio>
      </RadioGroup>
    )
  }
  radioResult.push(<p>Tout à fait d'accord</p>)
  return (
    <>
      <ContainerQuestionQuiz>{radioResult}</ContainerQuestionQuiz>
    </>
  )
}

export enum QuestionList {
  one = "Quel que soit son niveau d’accomplissement, un homme n’est pas vraiment « complet » en tant que personne s’il n’est pas aimé d’une femme.",
  two = "Sous l’apparence d’une politique d’égalité, beaucoup de femmes recherchent en fait des faveurs spéciales, comme un recrutement en entreprise qui les favorise.",
  three = "Lors d’une catastrophe, les femmes doivent être sauvées avant les hommes.",
  four = "La plupart des femmes interprètent des remarques ou des actes anodins comme étant sexistes.",
  five = "Les femmes sont trop rapidement offensées.",
  six = "Les gens ne sont pas vraiment heureux dans leur vie s’ils ne sont pas engagés dans une relation avec une personne de l’autre sexe.",
  seven = "Les féministes veulent que les femmes aient plus de pouvoir que les hommes.",
  eight = "Beaucoup de femmes ont une espèce de pureté que la plupart des hommes n’ont pas.",
  nine = "Les femmes devraient être protégées et être aimées par les hommes.",
  ten = "En général, une femme n’apprécie pas à sa juste valeur ce qu’un homme fait pour elle.",
  eleven = "Les femmes recherchent le pouvoir en ayant le contrôle sur les hommes.",
}
