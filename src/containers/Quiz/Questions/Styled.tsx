import styled from "styled-components"

export const QuizQuestion = styled.div`
  margin-bottom: 40px;
  border-bottom: 1px solid #d9d9d9;
`

export const QuizList = styled.div`
  display: flex;
`

export const ContainerQuestionQuiz = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 24px 0;
`

export const LabelRadio = styled.label`
  display: flex;
  align-items: center;
  flex-direction: column;
`
