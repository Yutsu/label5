import { QuizQuestion, QuizList } from "./Styled"
import { ResultQuizProps } from "./Type"
import { QuestionList, renderRadio } from "./Utils"

export const Questions: React.FC<ResultQuizProps> = ({ resultQuiz }) => {
  return (
    <form>
      <QuizQuestion>
        <QuizList>
          <strong>1. </strong> &nbsp; {QuestionList.one}
        </QuizList>
        {renderRadio(resultQuiz.one.quizOne, resultQuiz.one.setQuizOne)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>2. </strong> &nbsp; {QuestionList.two}
        </QuizList>
        {renderRadio(resultQuiz.two.quizTwo, resultQuiz.two.setQuizTwo)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>3. </strong> &nbsp; {QuestionList.three}
        </QuizList>
        {renderRadio(resultQuiz.three.quizThree, resultQuiz.three.setQuizThree)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>4. </strong> &nbsp; {QuestionList.four}
        </QuizList>
        {renderRadio(resultQuiz.four.quizFour, resultQuiz.four.setQuizFour)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>5. </strong> &nbsp; {QuestionList.five}
        </QuizList>
        {renderRadio(resultQuiz.five.quizFive, resultQuiz.five.setQuizFive)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>6. </strong> &nbsp; {QuestionList.six}
        </QuizList>
        {renderRadio(resultQuiz.six.quizSix, resultQuiz.six.setQuizSix)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>7. </strong> &nbsp; {QuestionList.seven}
        </QuizList>
        {renderRadio(resultQuiz.seven.quizSeven, resultQuiz.seven.setQuizSeven)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>8. </strong> &nbsp; {QuestionList.eight}
        </QuizList>
        {renderRadio(resultQuiz.eight.quizEight, resultQuiz.eight.setQuizEight)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>9. </strong> &nbsp; {QuestionList.nine}
        </QuizList>
        {renderRadio(resultQuiz.nine.quizNine, resultQuiz.nine.setQuizNine)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>10. </strong> &nbsp; {QuestionList.ten}
        </QuizList>
        {renderRadio(resultQuiz.ten.quizTen, resultQuiz.ten.setQuizTen)}
      </QuizQuestion>
      <QuizQuestion>
        <QuizList>
          <strong>11. </strong> &nbsp; {QuestionList.eleven}
        </QuizList>
        {renderRadio(
          resultQuiz.eleven.quizEleven,
          resultQuiz.eleven.setQuizEleven
        )}
      </QuizQuestion>
    </form>
  )
}
