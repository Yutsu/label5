export interface ResultQuizProps {
  resultQuiz: {
    one: {
      quizOne: string
      setQuizOne: any
    }
    two: {
      quizTwo: string
      setQuizTwo: any
    }
    three: {
      quizThree: string
      setQuizThree: any
    }
    four: {
      quizFour: string
      setQuizFour: any
    }
    five: {
      quizFive: string
      setQuizFive: any
    }
    six: {
      quizSix: string
      setQuizSix: any
    }
    seven: {
      quizSeven: string
      setQuizSeven: any
    }
    eight: {
      quizEight: string
      setQuizEight: any
    }
    nine: {
      quizNine: string
      setQuizNine: any
    }
    ten: {
      quizTen: string
      setQuizTen: any
    }
    eleven: {
      quizEleven: string
      setQuizEleven: any
    }
  }
}
