import styled from "styled-components"

export const Container = styled.div`
  display: flex;
  padding: 0 40px;
`

export const ContainerQuizz = styled.div`
  display: flex;
  box-shadow: 4px 4px 16px 2px rgba(0, 0, 0, 0.25);
  border-radius: 16px;
  margin-top: 100px;
  padding: 56px 136px;
  flex-direction: column;
  width: 100%;
  position: relative;
  overflow-y: scroll;
  height: calc(100vh - 300px);
`

export const ContainerButton = styled.div`
  width: 100px;
  border-radius: 16px;
  background-color: #8c6ab6;
  border: none;
  align-self: center;
`

export const ContainerRadioGroup = styled.label`
  display: flex;
  gap: 40px;
`

export const ContainerQuestionQuiz = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 24px 0;
`
