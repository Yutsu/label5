export interface SidebarProps {
  results: {
    quizOne: string
    quizTwo: string
    quizThree: string
    quizFour: string
    quizFive: string
    quizSix: string
    quizSeven: string
    quizEight: string
    quizNine: string
    quizTen: string
    quizEleven: string
  }
}
