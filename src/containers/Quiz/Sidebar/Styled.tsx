import styled from "styled-components"

interface ActiveStyled {
  active?: boolean
}

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 30%;
  margin-top: 100px;
  padding-right: 56px;
`

export const Image = styled.img`
  width: 100px;
  object-fit: "cover";
  margin-bottom: 24px;
`

export const Test = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

export const ImageAmfd = styled.img`
  object-fit: "cover";
`

export const ContainerAfmd = styled.div`
  display: flex;
  align-items: end;
  gap: 8px;
`

export const Text = styled.p`
  margin: 56px 0 20px;
  font-weight: bold;
`

export const ContainerRound = styled.div<ActiveStyled>`
  display: flex;
  gap: 24px;
  margin-bottom: 16px;
`

export const Round = styled.div<ActiveStyled>`
  width: 32px;
  height: 32px;
  border: 1px solid #6dc4c4ed;
  border-radius: 50%;
  position: relative;
  background-color: ${(props) => (props.active ? "#6dc4c4ed" : "white")};
`

export const Number = styled.div`
  color: ${(props) => (props.active ? "white" : "black")};
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`
