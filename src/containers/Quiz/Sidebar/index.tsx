import {
  Container,
  Image,
  ContainerAfmd,
  ImageAmfd,
  Text,
  Round,
  Number,
  ContainerRound,
} from "./Styled"
import { ResultQuizProps } from "../Type"

export const Sidebar: React.FC<ResultQuizProps> = ({ resultQuiz }) => {
  return (
    <Container>
      <Image src="/assets/presos_logo.svg" alt="presos company logo" />
      <ContainerAfmd>
        <strong>By</strong>
        <ImageAmfd src="/assets/afmd_logo.svg" alt="afmd logo" />
        <ImageAmfd src="/assets/afmd.svg" alt="afmd text logo" />
      </ContainerAfmd>
      <Text>Progression :</Text>
      <ContainerRound>
        <Round active={resultQuiz.one.quizOne}>
          <Number active={resultQuiz.one.quizOne}>1</Number>
        </Round>
        <Round active={resultQuiz.two.quizTwo}>
          <Number active={resultQuiz.two.quizTwo}>2</Number>
        </Round>
        <Round active={resultQuiz.three.quizThree}>
          <Number active={resultQuiz.three.quizThree}>3</Number>
        </Round>
        <Round active={resultQuiz.four.quizFour}>
          <Number active={resultQuiz.four.quizFour}>4</Number>
        </Round>
      </ContainerRound>
      <ContainerRound>
        <Round active={resultQuiz.five.quizFive}>
          <Number active={resultQuiz.five.quizFive}>5</Number>
        </Round>
        <Round active={resultQuiz.six.quizSix}>
          <Number active={resultQuiz.six.quizSix}>6</Number>
        </Round>
        <Round active={resultQuiz.seven.quizSeven}>
          <Number active={resultQuiz.seven.quizSeven}>7</Number>
        </Round>
        <Round active={resultQuiz.eight.quizEight}>
          <Number active={resultQuiz.eight.quizEight}>8</Number>
        </Round>
      </ContainerRound>
      <ContainerRound>
        <Round active={resultQuiz.nine.quizNine}>
          <Number active={resultQuiz.nine.quizNine}>9</Number>
        </Round>
        <Round active={resultQuiz.ten.quizTen}>
          <Number active={resultQuiz.ten.quizTen}>10</Number>
        </Round>
        <Round active={resultQuiz.eleven.quizEleven}>
          <Number active={resultQuiz.eleven.quizEleven}>11</Number>
        </Round>
      </ContainerRound>
    </Container>
  )
}
