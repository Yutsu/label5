import { Sidebar } from "./Sidebar"
import { Container, ContainerQuizz, ContainerButton } from "./Styled"
import { Button } from "../../components/Button"
import React, { useState } from "react"
import { useNavigate } from "react-router-dom"
import { Questions } from "./Questions"

export const Quiz = () => {
  const navigate = useNavigate()
  const [quizOne, setQuizOne] = useState(null)
  const [quizTwo, setQuizTwo] = useState(null)
  const [quizThree, setQuizThree] = useState(null)
  const [quizFour, setQuizFour] = useState(null)
  const [quizFive, setQuizFive] = useState(null)
  const [quizSix, setQuizSix] = useState(null)
  const [quizSeven, setQuizSeven] = useState(null)
  const [quizEight, setQuizEight] = useState(null)
  const [quizNine, setQuizNine] = useState(null)
  const [quizTen, setQuizTen] = useState(null)
  const [quizEleven, setQuizEleven] = useState(null)

  const handleClick = () => {
    console.log("quizOne", quizOne)
    console.log("quizTwo", quizTwo)
    if (
      quizOne &&
      quizTwo &&
      quizThree &&
      quizFour &&
      quizFive &&
      quizSix &&
      quizSeven &&
      quizEight &&
      quizNine &&
      quizTen &&
      quizEleven
    ) {
      navigate(`/finish`)
    }
  }

  const resultQuiz = {
    one: {
      quizOne,
      setQuizOne,
    },
    two: {
      quizTwo,
      setQuizTwo,
    },
    three: {
      quizThree,
      setQuizThree,
    },
    four: {
      quizFour,
      setQuizFour,
    },
    five: {
      quizFive,
      setQuizFive,
    },
    six: {
      quizSix,
      setQuizSix,
    },
    seven: {
      quizSeven,
      setQuizSeven,
    },
    eight: {
      quizEight,
      setQuizEight,
    },
    nine: {
      quizNine,
      setQuizNine,
    },
    ten: {
      quizTen,
      setQuizTen,
    },
    eleven: {
      quizEleven,
      setQuizEleven,
    },
  }

  return (
    <Container>
      <Sidebar resultQuiz={resultQuiz} />
      <ContainerQuizz>
        <Questions resultQuiz={resultQuiz} />
        <ContainerButton>
          <Button value="J'ai terminé" onClick={handleClick} />
        </ContainerButton>
      </ContainerQuizz>
    </Container>
  )
}
