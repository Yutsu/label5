import { useNavigate } from "react-router-dom"
import { Button } from "../../components/Button"
import { Container, ContainerButton, Item, List, ListUl, Span } from "./Styled"

export const Rules = () => {
  const navigate = useNavigate()

  const handleClick = () => {
    navigate(`/quiz`)
  }

  return (
    <Container>
      <p>
        Vous trouverez, ci-après, une série d’affirmations concernant les hommes
        et les femmes et les relations qu’ils/elles peuvent entretenir dans
        notre société. Indiquez dans quelle mesure vous êtes d’accord ou pas
        d’accord avec chacun des énoncés en utilisant la notation suivante :
      </p>
      <List>
        <ListUl>
          <li>0. Pas du tout d'accord</li>
          <li>1. Plutôt pas d'accord</li>
          <li>2. Légèrement pas d'accord</li>
        </ListUl>
        <ListUl>
          <Item>3. Légèrement d'accord</Item>
          <Item>4. Plutôt d'accord</Item>
          <Item>5. Tout à fait d'accord</Item>
        </ListUl>
      </List>
      <p>
        Il n’y a pas de bonnes ou mauvaises réponses. Toutes les réponses à ce
        questionnaire sont <Span>anonymes</Span>, seuls les résultats aggrégées
        au niveau de l’école seront pris en compte.
      </p>
      <p style={{ margin: "24px 0" }}>
        La durée du questionnaire est d’environ 15 minutes.
      </p>
      <ContainerButton>
        <Button value="C'est parti !" onClick={handleClick} />
      </ContainerButton>
    </Container>
  )
}
