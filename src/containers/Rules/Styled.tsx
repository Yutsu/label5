import styled from "styled-components"

export const Container = styled.div`
  position: absolute;
  top: 45vh;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 60%;
  border-radius: 16px;
  padding: 24px 32px;
  box-shadow: 4px 4px 16px 2px rgba(0, 0, 0, 0.25);
`
export const List = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 24px 0;
`
export const ListUl = styled.ul`
  list-style-type: none;
`

export const Item = styled.li`
  list-style-type: none;
`

export const ContainerButton = styled.div`
  display: flex;
  justify-content: center;
`

export const Span = styled.span`
  font-weight: bold;
`
