import {
  Container,
  ContainerAfmd,
  Containerr,
  Image,
  ImageAmfd,
  Text,
  Thanks,
} from "./Styled"

export const Finish = () => {
  return (
    <Container>
      <Image src="/assets/presos_logo.svg" alt="presos company logo" />
      <ContainerAfmd>
        <strong>By</strong>
        <ImageAmfd src="/assets/afmd_logo.svg" alt="afmd logo" />
        <ImageAmfd src="/assets/afmd.svg" alt="afmd text logo" />
      </ContainerAfmd>
      <Containerr>
        <Thanks>Merci !</Thanks>
        <Text>Vos réponses ont bien été enregistrées &nbsp; 🎉</Text>
      </Containerr>
    </Container>
  )
}
