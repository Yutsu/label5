import styled from "styled-components"

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 30%;
  padding-right: 56px;
  padding: 0 40px;
  position: relative;
  padding-top: 130px;
`

export const Image = styled.img`
  width: 100px;
  object-fit: "cover";
  margin-bottom: 24px;
`

export const ImageAmfd = styled.img`
  object-fit: "cover";
`

export const ContainerAfmd = styled.div`
  display: flex;
  align-items: end;
`

export const Containerr = styled.div`
  position: absolute;
  top: 45vh;
  left: 50vw;
  transform: translate(-50%, -50%);
  width: 100%;
  height: 360px;
  border-radius: 16px;
  padding: 24px 32px;
  box-shadow: 4px 4px 16px 2px rgba(0, 0, 0, 0.25);
  text-align: center;
  display: flex;
  align-items: center;
  flex-direction: column;
  box-shadow: 4px 4px 16px 2px rgba(0, 0, 0, 0.25);
`

export const Text = styled.p`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -55%);
`

export const Thanks = styled.h3`
  margin-top: 60px;
`
