import { Container, ContainerInner, HomeInner, Image, Logo } from "./Styled"
import { Login } from "../../containers/Login"

export const App = () => {
  return (
    <Container>
      <ContainerInner>
        <HomeInner active>
          <Logo>
            <Image src="/assets/afmd_logo.svg" alt="amfd logo" />
            <img src="/assets/afmd.svg" alt="amfd text logo" />
          </Logo>
          <br />
          <p>
            La diversité et l’inclusion, les seuls choix pour faire société
            demain
          </p>
        </HomeInner>
        <HomeInner>
          <Login />
        </HomeInner>
      </ContainerInner>
    </Container>
  )
}
