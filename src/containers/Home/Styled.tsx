import styled from "styled-components"

interface ActiveStyled {
  active?: boolean
}

export const Container = styled.div`
  display: flex;
  margin: 0;
  padding: 0;
`

export const ContainerInner = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export const HomeInner = styled.div<ActiveStyled>`
  width: 50%;
  height: 100vh;
  justify-content: center;
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: ${(props) => (props.active ? "#E8F7F7" : "white")};
`

export const Logo = styled.div`
  justify-content: center;
  gap: 8px;
`

export const Lorem = styled.div`
  margin-top: 80px;
  font-size: 16px;
  width: 1300px;
  padding: 0 16px;
`

export const Image = styled.img`
  margin-right: 16px;
`
