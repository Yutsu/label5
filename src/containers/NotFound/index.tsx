import { Container } from "./Styled"

export const NotFound = () => {
  return (
    <Container>
      <h1>La page que vous cherchez n'existe pas !</h1>
      <img src="/assets/not_found.svg" alt="not found draw from undraw" />
    </Container>
  )
}
