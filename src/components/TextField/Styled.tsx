import styled from "styled-components"

export const Container = styled.div`
  display: "flex";
  flex-direction: "column";
  width: 200px;
  margin-bottom: 30px;
`

export const Input = styled.input`
  padding: 4px 8px;
  border-radius: 4px;
  border: 1px solid #232844;
`

export const Description = styled.div`
  font-size: 10px;
`

export const ErrorMessage = styled.div`
  color: red;
  font-size: 12px;
`

export const ContainerTextField = styled.div`
  display: "flex";
  flex-direction: "column";
`

export const Label = styled.label``
