import { useTextField } from "@react-aria/textfield"
import React, { useRef } from "react"
import {
  Container,
  ContainerTextField,
  Description,
  ErrorMessage,
  Input,
  Label,
} from "./Styled"
import { TextFieldProps } from "./Type"

export const TextField: React.FC<TextFieldProps> = ({
  label,
  description,
  errorMessage,
  value,
  onChange,
  type,
}) => {
  const ref = useRef()
  const { labelProps, inputProps, descriptionProps, errorMessageProps } =
    useTextField({ label, description, errorMessage }, ref)

  const renderDescription = () => {
    if (description) {
      return <Description {...descriptionProps}>{description}</Description>
    }
  }

  const renderErrorMessage = () => {
    if (errorMessage) {
      if (errorMessage === "mail must be a valid email") {
        return (
          <ErrorMessage {...errorMessageProps}>
            Le mail doit être un mail valide
          </ErrorMessage>
        )
      }
      return (
        <ErrorMessage {...errorMessageProps}>
          Le mot de passe doit être requis
        </ErrorMessage>
      )
    }
  }

  return (
    <Container>
      <ContainerTextField style={{ display: "flex", flexDirection: "column" }}>
        <Label {...labelProps}>{label}</Label>
        <Input
          {...inputProps}
          ref={ref}
          onChange={onChange}
          value={value}
          type={type}
        />
      </ContainerTextField>
      {renderDescription()}
      {renderErrorMessage()}
    </Container>
  )
}
