import { useButton } from "@react-aria/button"
import { useRef } from "react"
import { Input } from "./Styled"

export const Button = (props: any) => {
  const ref = useRef()
  const { buttonProps } = useButton(props, ref)

  return <Input {...buttonProps} ref={ref} type="submit" value={props.value} />
}
