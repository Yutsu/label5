import styled from "styled-components"

export const Input = styled.input`
  padding: 8px 24px;
  background-color: #8c6ab6;
  color: white;
  border: none;
  border-radius: 8px;
  cursor: pointer;
`
