import { ReactNode } from "react"

export interface RadioGroupProps {
  label?: string
  children: ReactNode
}
