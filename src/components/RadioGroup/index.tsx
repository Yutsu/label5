import React from "react"
import { useRadioGroup } from "@react-aria/radio"
import { useRadioGroupState } from "@react-stately/radio"
import { ContainerRadioGroup } from "../../containers/Quiz/Styled"
import { RadioContext } from "../Radio/Type"

export const RadioGroup = (props) => {
  const { children, label } = props
  const state = useRadioGroupState(props)
  const { radioGroupProps, labelProps } = useRadioGroup(props, state)

  return (
    <ContainerRadioGroup {...radioGroupProps}>
      <span {...labelProps}>{label}</span>
      <RadioContext.Provider value={state}>{children}</RadioContext.Provider>
    </ContainerRadioGroup>
  )
}
