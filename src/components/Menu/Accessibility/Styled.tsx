import styled from "styled-components"

interface ActiveStyled {
  bgDark?: boolean
  transformAnimation?: boolean
}

export const Container = styled.div<ActiveStyled>`
  display: flex;
  justify-content: space-between;
  width: calc(100% - 90px);
  height: 32px;
  padding: 16px 16px 16px 0;
  overflow: scroll;
  position: absolute;
  top: 0;
  right: 0;
  gap: 16px;
  z-index: 1;
  transform: ${(props) =>
    props.transformAnimation ? "translateY(0)" : "translateY(-200%)"};
  transition: 0.4s;
`

export const ContainerMenu = styled.button`
  display: flex;
  align-items: center;
  gap: 8px;
  white-space: nowrap;
  cursor: pointer;
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  /* outline: inherit; */
`

export const Arrow = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  border: 2px solid black;
  /* outline: inherit; */
  box-sizing: border-box;
  cursor: pointer;
`

export const ContainerArrow = styled.button`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 16px;
  /* transform: translateY(-50%); */
  left: 24px;
  width: 32px;
  height: 32px;
  cursor: pointer;
  background: none;
  color: inherit;
  padding: 0;
  font: inherit;
  border: none;
  z-index: 1;
  transition: 0.3s;
  /* outline: inherit; */
`

export const Line = styled.div<ActiveStyled>`
  display: flex;
  justify-content: space-between;
  width: 100%;
  height: 64px;
  top: 0;
  position: fixed;
  box-shadow: 0px 10px 15px -7px rgba(0, 0, 0, 0.2);
  z-index: 0;
  box-sizing: border-box;
  border-radius: 0 0 16px 16px;
  background-color: ${(props) => (props.bgDark ? "black" : "white")};
  transform: ${(props) =>
    props.transformAnimation ? "translateY(0)" : "translateY(-200%)"};
  transition: 0.3s;

  @media screen and (max-width: 1405px) {
    border-radius: 0 0 0 16px;
  }
`
