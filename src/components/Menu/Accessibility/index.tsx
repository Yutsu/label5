import React, { useRef, useState } from "react"
import ZoomInIcon from "@mui/icons-material/ZoomIn"
import ZoomOutIcon from "@mui/icons-material/ZoomOut"
import GradientIcon from "@mui/icons-material/Gradient"
import ContrastIcon from "@mui/icons-material/Contrast"
import VisibilityIcon from "@mui/icons-material/Visibility"
import EmojiObjectsOutlinedIcon from "@mui/icons-material/EmojiObjectsOutlined"
import FontDownloadOutlinedIcon from "@mui/icons-material/FontDownloadOutlined"
import RestartAltOutlinedIcon from "@mui/icons-material/RestartAltOutlined"
import AccessibilityNewRoundedIcon from "@mui/icons-material/AccessibilityNewRounded"
import { Arrow, Container, ContainerArrow, ContainerMenu, Line } from "./Styled"
import { useButton } from "@react-aria/button"
import { GlobalStyle } from "../../../Styled"
import { render } from "@testing-library/react"

export const Accessibility = (props) => {
  const [fontSize, setFontSize] = useState(100)
  const [gradient, setGradient] = useState(false)
  const [highContrast, setHighContrast] = useState(false)
  const [negContrast, setNegContrast] = useState(false)
  const [bgDark, setBgDark] = useState(false)
  const [fontFamily, setFontFamily] = useState(false)
  const [transformAnimation, setTransformAnimation] = useState(false)

  const handleClick = () => {
    setTransformAnimation(!transformAnimation)
  }

  const zoomInAccess = () => {
    fontSize <= 105 && setFontSize((fontSize) => fontSize + 1)

    render(
      <GlobalStyle
        fontsize={fontSize + 1 + "%"}
        gradient={gradient}
        highContrast={highContrast}
        negContrast={negContrast}
        bgDark={bgDark}
        fontFamily={fontFamily}
      />
    )
  }
  const zoomOutAccess = () => {
    console.log("zoomout")
    fontSize >= 95 && setFontSize((fontSize) => fontSize - 1)

    render(
      <GlobalStyle
        fontsize={fontSize - 1 + "%"}
        gradient={gradient}
        highContrast={highContrast}
        negContrast={negContrast}
        bgDark={bgDark}
        fontFamily={fontFamily}
      />
    )
  }
  const gradientAccess = () => {
    setGradient(!gradient)

    render(
      <GlobalStyle
        fontsize={fontSize}
        gradient={!gradient}
        highContrast={highContrast}
        negContrast={negContrast}
        bgDark={bgDark}
        fontFamily={fontFamily}
      />
    )
  }
  const highContrastAccess = () => {
    setHighContrast(!highContrast)

    render(
      <GlobalStyle
        fontsize={fontSize}
        gradient={gradient}
        highContrast={!highContrast}
        negContrast={negContrast}
        bgDark={bgDark}
        fontFamily={fontFamily}
      />
    )
  }
  const negContrastAccess = () => {
    setNegContrast(!negContrast)

    render(
      <GlobalStyle
        fontsize={fontSize}
        gradient={gradient}
        highContrast={highContrast}
        negContrast={!negContrast}
        bgDark={bgDark}
        fontFamily={fontFamily}
      />
    )
  }
  const bgDarkAccess = () => {
    setBgDark(!bgDark)

    render(
      <GlobalStyle
        fontsize={fontSize}
        gradient={gradient}
        highContrast={highContrast}
        negContrast={negContrast}
        bgDark={!bgDark}
        fontFamily={fontFamily}
      />
    )
  }
  const readableFontAccess = () => {
    setFontFamily(!fontFamily)

    render(
      <GlobalStyle
        fontsize={fontSize}
        gradient={gradient}
        highContrast={highContrast}
        negContrast={negContrast}
        bgDark={bgDark}
        fontFamily={!fontFamily}
      />
    )
  }
  const reinitializeAccess = () => {
    setNegContrast(false)

    render(
      <GlobalStyle
        fontsize={100 + "%"}
        gradient={false}
        highContrast={false}
        negContrast={false}
        bgDark={false}
        fontFamily={false}
      />
    )
  }

  const data = [
    {
      icon: <ZoomInIcon />,
      text: "Augmenter le texte",
      function: zoomInAccess,
    },
    {
      icon: <ZoomOutIcon />,
      text: "Diminuer le texte",
      function: zoomOutAccess,
    },
    {
      icon: <GradientIcon />,
      text: "Niveau de gris",
      function: gradientAccess,
    },
    {
      icon: <ContrastIcon />,
      text: "Haut contraste",
      function: highContrastAccess,
    },
    {
      icon: <VisibilityIcon />,
      text: "Contraste négatif",
      function: negContrastAccess,
    },
    {
      icon: <EmojiObjectsOutlinedIcon />,
      text: "Arrière-plan foncé",
      function: bgDarkAccess,
    },
    {
      icon: <FontDownloadOutlinedIcon />,
      text: "Police lisible",
      function: readableFontAccess,
    },
    {
      icon: <RestartAltOutlinedIcon />,
      text: "Réinitialiser",
      function: reinitializeAccess,
    },
  ]

  const ref = useRef()
  const { buttonProps } = useButton(props, ref)

  const renderData = () => {
    return data.map((el, i) => {
      return (
        <ContainerMenu {...buttonProps} onClick={el.function} key={i}>
          {el.icon}
          {el.text}
        </ContainerMenu>
      )
    })
  }

  return (
    <>
      <ContainerArrow>
        <Arrow {...buttonProps} onClick={handleClick}>
          <AccessibilityNewRoundedIcon />
        </Arrow>
      </ContainerArrow>
      <Container transformAnimation={transformAnimation}>
        {renderData()}
      </Container>
      <Line bgDark={negContrast} transformAnimation={transformAnimation} />
    </>
  )
}
