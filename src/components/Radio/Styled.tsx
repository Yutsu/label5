import styled from "styled-components"

export const LabelRadio = styled.label`
  display: flex;
  align-items: center;
  flex-direction: column;
`
