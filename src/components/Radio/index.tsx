import React, { useContext, useRef } from "react"
import { VisuallyHidden } from "@react-aria/visually-hidden"
import { useFocusRing } from "@react-aria/focus"
import { useRadio } from "@react-aria/radio"
import { LabelRadio } from "./Styled"
import { RadioContext, RadioProps } from "./Type"

export const Radio: React.FC<RadioProps> = (props) => {
  const { children } = props
  const state = useContext(RadioContext)
  const ref = useRef(null)
  const { inputProps } = useRadio(props, state, ref)
  const { isFocusVisible, focusProps } = useFocusRing()
  const isSelected = state.selectedValue === props.value
  const strokeWidth = isSelected ? 6 : 2

  return (
    <LabelRadio>
      <VisuallyHidden>
        <input {...inputProps} {...focusProps} ref={ref} />
      </VisuallyHidden>
      <svg width={24} height={24} aria-hidden="true">
        <circle
          cx={12}
          cy={12}
          r={8 - strokeWidth / 2}
          fill="none"
          stroke={isSelected ? "#6DC4C4" : "gray"}
          strokeWidth={strokeWidth}
        />
        {isFocusVisible && (
          <circle
            cx={12}
            cy={12}
            r={11}
            fill="none"
            stroke="#6DC4C4"
            strokeWidth={2}
          />
        )}
      </svg>
      {children}
    </LabelRadio>
  )
}
