import { createContext, ReactNode } from "react"
import { AriaRadioProps } from "@react-types/radio"

export const RadioContext = createContext(null)

export interface RadioProps extends AriaRadioProps {
  children: ReactNode
}
