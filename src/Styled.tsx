import { createGlobalStyle } from "styled-components"

interface ActiveStyled {
  fontsize?: String
  gradient?: Boolean
  highContrast?: Boolean
  negContrast?: Boolean
  bgDark?: Boolean
  fontFamily?: Boolean
}

export const GlobalStyle = createGlobalStyle<ActiveStyled>`
* {
  margin: 0;
  padding: 0;
  font-family: ${(props) => (props.fontFamily ? "SB" : "Regular")};
  font-size: ${(props) => props.fontsize || "100%"};
  filter: ${(props) =>
    props.gradient
      ? "saturate(0)"
      : "saturate(1)" && props.negContrast
      ? "invert(1)"
      : "invert(0)"};

  color: ${(props) => (props.highContrast ? "#0080FF" : "inherit")};
}

/* h1, h2, h3, h4 {
  font-family: 'Quicksand';
} */

body{
  background: ${(props) => (props.bgDark ? "black" : "white")};
  line-height: 20px;
}

`
