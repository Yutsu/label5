const mongoose = require("mongoose")
const { Schema } = mongoose
const bcrypt = require("bcryptjs")
const { isEmail } = require("validator")

const userSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      minlength: 3,
    },
    mail: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      validate: [isEmail],
      lowercase: true,
      minlength: 3,
    },
    password: {
      type: String,
      required: true,
      minlength: 6,
    },
  },
  { timestamps: true }
)

userSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    bcrypt.hash(this.password, 10, (err, hash) => {
      if (err) return next(err)

      this.password = hash
      next()
    })
  }
})

userSchema.methods = {
  comparePassword: (password) => {
    if (!password) throw new Error("Password is mission, can not compare")

    try {
      const result = bcrypt.compare(password, this.password)

      return result
    } catch (err) {
      return err
    }
  },
}

const User = mongoose.model("User", userSchema)

module.exports = User
