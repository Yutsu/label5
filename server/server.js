const express = require("express")
const app = express()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const mongoose = require("mongoose")
require("dotenv").config()
const usersRouter = require("./routes/users.routes.js")
const uri = process.env.ATLAS_URI
const connection = mongoose.connection

const PORT = process.env.PORT || 6000

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

mongoose.connect(uri, { useNewUrlParser: true })

connection.once("open", () => {
  console.log("Database MongoDB connectée")
})

const verifyToken = (req, res, next) => {
  const idToken = req.headers.authorization
  console.log("coucouuu")

  jwt.verify(idToken, process.env.PUBLIC_KEY, (err, decoded) => {
    if (err) {
      res.status(401).send("Non autorisé")
    } else {
      next()
    }
  })
}

app.use("/users", usersRouter)
app.use("/rulesQuizz", verifyToken)

app.listen(PORT, () => {
  console.log(`Serveur connecté au port: ${PORT}`)
})
