const router = require("express").Router()
const User = require("../models/user.model")
const authController = require("../controllers/auth.controller")
const { check } = require("express-validator")

router.route("/").get((_, res) => {
  User.find()
    .then((users) => res.json(users))
    .catch((err) => res.status(400).json("Error: " + err))
})

router.route("/:id").get((req, res) => {
  User.findById(req.params.id)
    .then((user) => res.json(user))
    .catch((err) => res.status(400).json("Error: " + err))
})

router.post("/register", authController.signUp)
router.post(
  "/login",
  [
    check("mail", "Il faut 3 caractères minimum").isLength({ min: 3 }),
    check("password", "Il faut 3 caractères minimum").isLength({ min: 3 }),
  ],
  authController.signIn
)

module.exports = router
