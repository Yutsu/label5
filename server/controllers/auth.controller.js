const User = require("../models/user.model")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
require("dotenv").config()
const expressJwt = require("express-jwt")

module.exports.signUp = async (req, res) => {
  const { username, mail, password } = req.body

  try {
    const user = await User.create({ username, mail, password })
    res.status(201).json({ user: user._id })
  } catch (error) {
    res.status(200).send({ error })
  }
}

module.exports.signIn = async (req, res) => {
  const { mail, password } = req.body

  try {
    const user = await User.findOne({ mail })

    if (!user) {
      return res.json({
        status: 401,
        error: "Invalide username/email/password",
      })
    }

    if (!(await bcrypt.compareSync(password, user.password))) {
      return res.json({
        status: 401,
        error: "Invalid password",
      })
    }

    const accessToken = jwt.sign({ _id: user._id }, process.env.PRIVATE_KEY)

    res.cookie("token", accessToken, { expire: new Date() + 1 })

    const { _id, username } = user

    return res
      .status(200)
      .send({ status: 200, accessToken, user: { _id, username, mail } })
  } catch (error) {
    return res.status(404).send({ status: 404, error })
  }
}
